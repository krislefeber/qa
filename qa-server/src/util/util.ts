class AsyncArray<T> extends Array<T> {

  public static from<T>(items?: T[]): AsyncArray<T> {
    return new AsyncArray(items);
  }

  private constructor(items?: T[]) {
    super(...items);
  }

  public async asyncSome(
    callback: (value: T, index: number, array: T[]) => Promise<boolean>,
  ): Promise<boolean> {
    for (let index = 0; index < this.length; index++) {
      if (await callback(this[index], index, this)) {
        return true;
      }
    }
    return false;
  }
  public async asyncFilter(
    callback: (value: T, index: number, array: T[]) => Promise<boolean>,
  ): Promise<T[]> {
    let filtered: T[] = [];
    for (let index = 0; index < this.length; index++) {
      if (await callback(this[index], index, this)) {
        filtered = [...filtered, this[index]];
      }
    }
    return filtered;
  }
  public async asyncForEach(
    callback: (value: T, index: number, array: T[]) => Promise<void>,
  ): Promise<void> {
    for (let index = 0; index < this.length; index++) {
      await callback(this[index], index, this);
    }
    return Promise.resolve();
  }

  public async asyncMap<K>(
    callback: (value: T, index: number, array: T[]) => Promise<K>,
  ): Promise<K[]> {
    const arr = [];
    for (let index = 0; index < this.length; index++) {
        arr[index] =  await callback(this[index], index, this);
    }
    return arr;
  }

  public myFlatMap<E>(c: (t: T) => E[]): E[] {
    return this.reduce((ys: any, x: any) => {
        return ys.concat(c.call(this, x));
    }, []as E[]);
  }

}

export function isUUID(val: string): boolean {
  return  !!/[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}/.exec(val);
}

export default AsyncArray;
