"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class DuplicateUserException extends Error {
    constructor(username) {
        super(`The username ${username} is no longer available. Please use a different username.`);
    }
}
exports.DuplicateUserException = DuplicateUserException;
//# sourceMappingURL=DuplicateUserException.js.map