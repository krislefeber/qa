import gql from 'graphql-tag';
const answerView = gql`
  fragment AnswerView on Answer {
    id
    createdOn
    votes
    response
    createdBy {
      id
    }
  }`
;
const QuestionPage = {
  fragments: {
    question: gql`
      fragment QuestionView on Question {
        id
        createdOn
        title
        description
        votes
        tags {
          id
          name
        }
        createdBy {
          id
        }
        answers {
          ...AnswerView
        }
      }
      ${answerView}`,
      answer: answerView
  }
};

export const QUESTION_VIEW_QUERY = gql`
query getQuestionView($id:String!) {
  question(id:$id) {
    ...QuestionView
  }
}
${QuestionPage.fragments.question}`;

export const ANSWER_QUESTION_MUTATION = gql`
mutation addAnswerMutation($questionId: String!, $answer: String!) {
  answerQuestion(questionId:$questionId, answer: $answer) {
    ...QuestionView
  }
}
${QuestionPage.fragments.question}`;

export const UPVOTE_ANSWER_MUTATION = gql`
mutation upVoteAnswer($answerId:String!) {
  upVoteAnswer(answerId:$answerId) {
    ...AnswerView
  }
}
${QuestionPage.fragments.answer}
`;

export const DOWNVOTE_ANSWER_MUTATION = gql`
mutation downVoteAnswer($answerId:String!) {
  downVoteAnswer(answerId:$answerId) {
    ...AnswerView
  }
}
${QuestionPage.fragments.answer}
`;
