

/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: downVoteAnswer
// ====================================================

export interface downVoteAnswer_downVoteAnswer_createdBy {
  __typename: "Account";
  id: string;
}

export interface downVoteAnswer_downVoteAnswer {
  __typename: "Answer";
  id: string;
  createdOn: any;
  votes: number;
  response: string;
  createdBy: downVoteAnswer_downVoteAnswer_createdBy | null;
}

export interface downVoteAnswer {
  downVoteAnswer: downVoteAnswer_downVoteAnswer;
}

export interface downVoteAnswerVariables {
  answerId: string;
}

/* tslint:disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

/**
 * 
 */
export interface QuestionSearchInput {
  search?: string | null;
}

//==============================================================
// END Enums and Input Objects
//==============================================================