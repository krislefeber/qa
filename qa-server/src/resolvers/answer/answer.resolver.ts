import { Arg, Mutation } from 'type-graphql';
import { Inject } from 'typedi';
import { Answer } from '../../models/answer.model';
import { Question } from '../../models/question.model';
import { AnswerService } from '../../services/answer.service';

export class AnswerResolver {

  @Inject(() => AnswerService)
  private answerService: AnswerService;

  @Mutation(() => Question)
  public async answerQuestion(@Arg('questionId') questionId: string, @Arg('answer') answer: string) {
    return this.answerService.addAnswer(questionId, answer);
  }

  @Mutation(() => Answer)
  public async upVoteAnswer(@Arg('answerId') id: string) {
    return this.answerService.vote(id, true);
  }

  @Mutation(() => Answer)
  public async downVoteAnswer(@Arg('answerId') id: string) {
    return this.answerService.vote(id, false);
  }
}
