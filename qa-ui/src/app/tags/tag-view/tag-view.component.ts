import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'apollo-link';
@Component({
  selector: 'app-tag-view',
  templateUrl: './tag-view.component.html',
  styleUrls: ['./tag-view.component.scss']
})
export class TagViewComponent implements OnInit {

  @Input() tag: Tag;
  @Input() clickedTag: Observable<Tag>;
  @Output() tagRemoved: EventEmitter<Tag> = new EventEmitter();
  @Output() tagClicked: EventEmitter<Tag> = new EventEmitter();
  isEditable: boolean;

  constructor() { }

  ngOnInit() {
    this.clickedTag.subscribe(val => {
      if (val.id !== this.tag.id) {
        this.isEditable = false;
      }
    });
  }

  handleRemoval() {
    this.tagRemoved.emit(this.tag);
  }

  toggleEditable() {
    this.isEditable = !this.isEditable;
    if (this.isEditable) {
      this.tagClicked.emit(this.tag);
    }
  }
}


export interface Tag {
  id: string;
  name: string;
}
