import { Component, OnInit, Input } from '@angular/core';
import { searchQuestions_search } from '__generated__/searchQuestions';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.scss']
})
export class SearchResultsComponent implements OnInit {

  @Input() searchResults: searchQuestions_search[];

  constructor() { }

  ngOnInit() {
  }

  truncate(text: string, length: number) {
  return text.length > length ? text.substr(0, length) + '...' : text;
  }
}
