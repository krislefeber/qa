import { DeleteResult, RemoveOptions, Repository } from 'typeorm';
import { SoftDeletable } from '../models/softdeletable';

export class BaseRepository<T extends SoftDeletable> extends Repository<T> {

  public createQueryBuilder(baseName: string) {
    return super.createQueryBuilder(baseName).where(`${baseName}.softDeleted = false`);
  }

  public async delete(criteria: string, options?: RemoveOptions): Promise<DeleteResult> {
    return this.softDelete(criteria) as any;
  }

  public async softDelete(criteria: string): Promise<T> {
    const val = await this.findOne(criteria);
    val.softDeleted = true;
    return this.save(val as any);
  }
}
