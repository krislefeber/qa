import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {

  searchForm: FormGroup;
  @Output()
  questionRequested = new EventEmitter();

  @Output()
  searchChange: EventEmitter<Search> = new EventEmitter(true);

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.searchForm = this.formBuilder.group({
      search: ['']
    });
  }

  handleSearch() {
    this.searchChange.emit(this.searchForm.value);
  }

  addQuestion() {
    this.questionRequested.emit();
  }

}

export interface Search {
  search: string;
}
