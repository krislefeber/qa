import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Search } from '../search/search-view/search-form/search-form.component';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  private currentSearch: BehaviorSubject<Search> = new BehaviorSubject({ search: '*' });

  constructor(private router: Router) { }

  updateSearch(text: Search) {
    console.log('updateSearch', text);
    this.router.navigate(['/search'], {
      queryParams: text,
    }).then((result) =>
     console.log(result),
      err =>
      console.log(err));
    this.currentSearch.next(text);
  }

  currentSearchObservable() {
    return this.currentSearch.asObservable();
  }

}
