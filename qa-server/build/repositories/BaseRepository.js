"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
class BaseRepository extends typeorm_1.Repository {
    createQueryBuilder(baseName) {
        return super.createQueryBuilder(baseName).where(`${baseName}.softDeleted = false`);
    }
    delete(criteria, options) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.softDelete(criteria);
        });
    }
    softDelete(criteria) {
        return __awaiter(this, void 0, void 0, function* () {
            const val = yield this.findOne(criteria);
            val.softDeleted = true;
            return this.save(val);
        });
    }
}
exports.BaseRepository = BaseRepository;
//# sourceMappingURL=BaseRepository.js.map