import { Field, InputType } from 'type-graphql';

@InputType()
export class QuestionSearchInput {

  @Field({nullable: true})
  public search: string;
}
