
export class DuplicateUserException extends Error {

  constructor(username: string) {
    super(`The username ${username} is no longer available. Please use a different username.`);
}
}
