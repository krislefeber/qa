import { EntityRepository } from 'typeorm';
import { Question } from '../models/question.model';
import { BaseRepository } from './BaseRepository';

@EntityRepository(Question)
export class QuestionRepository extends BaseRepository<Question> {

}
