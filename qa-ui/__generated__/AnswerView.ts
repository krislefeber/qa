

/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: AnswerView
// ====================================================

export interface AnswerView_createdBy {
  __typename: "Account";
  id: string;
}

export interface AnswerView {
  __typename: "Answer";
  id: string;
  createdOn: any;
  votes: number;
  response: string;
  createdBy: AnswerView_createdBy | null;
}

/* tslint:disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

/**
 * 
 */
export interface QuestionSearchInput {
  search?: string | null;
}

//==============================================================
// END Enums and Input Objects
//==============================================================