import { Arg, Ctx, Mutation, Query, Resolver, FieldResolver, Root } from 'type-graphql';
import { Inject } from 'typedi';
import { NoEntityFoundException } from '../../exceptions/NoEntityFoundException';
import { Question } from '../../models/question.model';
import { QuestionService } from '../../services/question.service';
import { QuestionSearchInput } from './question-search.input';

@Resolver(() => Question)
export class QuestionResolver {

  @Inject(() => QuestionService)
  private questionService: QuestionService;

  @Query(() => Question)
  public async question(@Arg('id') id: string) {
    const account = await this.questionService.findById(id);
    if (account === undefined) {
      throw new NoEntityFoundException(Question, id);
    }
    return account;
  }

  @Mutation(() => Question)
  public async createQuestion(@Arg('title') title: string, @Arg('description') description: string) {
    return this.questionService.createQuestion(undefined, title, description);
  }

  @Mutation(() => Question)
  public async addTag(@Arg('tagId', {
    description: 'The ID of an existing tag or the name of a new tag',
  }) tag: string,     @Arg('questionId') questionId: string) {
    return this.questionService.addTag(tag, questionId);
  }

  @Mutation(() => Question)
  public async removeTag(@Arg('tagId', {
            description: 'The ID of an existing tag',
         }) tag: string, @Arg('questionId') questionId: string) {
    return this.questionService.removeTag(tag, questionId);
  }

  @Query(() => [Question])
  public async search(@Arg('searchInpt') searchInput: QuestionSearchInput): Promise<Question[]> {
    return this.questionService.search(searchInput);
  }

  @FieldResolver(() => Number)
  public async numberOfAnswers(@Root() question: Question) {
    return (await question.answers).length;
  }
}
