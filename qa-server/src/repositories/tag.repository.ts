import { EntityRepository } from 'typeorm';
import { Tag } from '../models/tag.model';
import { BaseRepository } from './BaseRepository';

@EntityRepository(Tag)
export class TagRepository extends BaseRepository<Tag> {

  public async findByName(name: string) {
    return this.findOne({
      name,
    });
  }
}
