import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchRoutingModule } from './search-routing.module';
import { SearchViewComponent } from './search-view/search-view.component';
import { SearchFormComponent } from './search-view/search-form/search-form.component';
import { FlexModule, FlexLayoutModule } from '@angular/flex-layout';
import { MatInputModule, MatIconModule, MatButtonModule, MatCardModule, MatMenuModule, MatTooltipModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SearchResultsComponent } from './search-view/search-results/search-results.component';

import { AddQuestionComponent } from './add-question/add-question.component';
import { ApolloModule } from 'apollo-angular';

@NgModule({
  declarations: [SearchViewComponent, SearchFormComponent, SearchResultsComponent, AddQuestionComponent],
  imports: [
    ApolloModule,
    CommonModule,
    FlexModule,
    ReactiveFormsModule,
    SearchRoutingModule,
    FlexLayoutModule,
    // Angular Material
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatInputModule,
    MatTooltipModule,
  ],
  exports: [
    SearchFormComponent
  ]
})
export class SearchModule { }
