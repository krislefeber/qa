import { Field, ID, ObjectType } from 'type-graphql';
import { Column, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Account } from './account.model';
import { Answer } from './answer.model';
import { SoftDeletable } from './softdeletable';
import { Tag } from './tag.model';
@ObjectType({ implements: SoftDeletable})
@Entity()
export class Question extends SoftDeletable {

  @Field(() => ID)
  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @Field()
  @Column()
  public title: string;

  @Field()
  @Column()
  public description: string;

  @Field()
  @Column({
    default: 0,
  })
  public votes: number;

  @Field(() => Account, {
    nullable: true,
  })
  @ManyToOne((type) => Account, {lazy: true, nullable: true})
  public createdBy: Promise<Account>;

  @Field(() => [Answer], {
    nullable: true,
  })
  @OneToMany((type) => Answer, (answer) => answer.question, {lazy: true, nullable: true})
  public answers: Promise<Answer[]>;

  @Field(() => [Tag], { nullable: true})
  @ManyToMany(() => Tag, {
    cascade: ['insert', 'update'],
    eager: true,
    onUpdate: 'CASCADE',
  })
  @JoinTable()
  public tags: Promise<Tag[]>;
}
