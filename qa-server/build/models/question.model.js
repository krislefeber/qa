"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const type_graphql_1 = require("type-graphql");
const typeorm_1 = require("typeorm");
const account_model_1 = require("./account.model");
const answer_model_1 = require("./answer.model");
const softdeletable_1 = require("./softdeletable");
const tag_model_1 = require("./tag.model");
let Question = class Question extends softdeletable_1.SoftDeletable {
};
__decorate([
    type_graphql_1.Field(() => type_graphql_1.ID),
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Question.prototype, "id", void 0);
__decorate([
    type_graphql_1.Field(),
    typeorm_1.Column(),
    __metadata("design:type", String)
], Question.prototype, "title", void 0);
__decorate([
    type_graphql_1.Field(),
    typeorm_1.Column(),
    __metadata("design:type", String)
], Question.prototype, "description", void 0);
__decorate([
    type_graphql_1.Field(),
    typeorm_1.Column({
        default: 0,
    }),
    __metadata("design:type", Number)
], Question.prototype, "votes", void 0);
__decorate([
    type_graphql_1.Field(() => account_model_1.Account, {
        nullable: true,
    }),
    typeorm_1.ManyToOne((type) => account_model_1.Account, { lazy: true, nullable: true }),
    __metadata("design:type", Promise)
], Question.prototype, "createdBy", void 0);
__decorate([
    type_graphql_1.Field(() => [answer_model_1.Answer], {
        nullable: true,
    }),
    typeorm_1.OneToMany((type) => answer_model_1.Answer, (answer) => answer.question, { lazy: true, nullable: true }),
    __metadata("design:type", Promise)
], Question.prototype, "answers", void 0);
__decorate([
    type_graphql_1.Field(() => [tag_model_1.Tag], { nullable: true }),
    typeorm_1.ManyToMany(() => tag_model_1.Tag, {
        cascade: ['insert', 'update'],
        lazy: false,
        onUpdate: 'CASCADE',
    }),
    typeorm_1.JoinTable(),
    __metadata("design:type", Promise)
], Question.prototype, "tags", void 0);
Question = __decorate([
    type_graphql_1.ObjectType({ implements: softdeletable_1.SoftDeletable }),
    typeorm_1.Entity()
], Question);
exports.Question = Question;
//# sourceMappingURL=question.model.js.map