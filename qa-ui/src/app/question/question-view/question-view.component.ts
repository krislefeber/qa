import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { Apollo} from 'apollo-angular';
import { QUESTION_VIEW_QUERY } from './question-view.graphql';
import { getQuestionView, getQuestionViewVariables,
         getQuestionView_question, getQuestionView_question_answers } from '__generated__/getQuestionView';
import {map} from 'rxjs/operators';
import { Location } from '@angular/common';
@Component({
  selector: 'app-question-view',
  templateUrl: './question-view.component.html',
  styleUrls: ['./question-view.component.scss']
})
export class QuestionViewComponent implements OnInit {

  question: Observable<getQuestionView_question> = new Observable(undefined);


  constructor(private route: ActivatedRoute,
              private apollo: Apollo) { }

  ngOnInit() {
    this.route.params.subscribe((val: Params) => {
      console.log('loading question ' + val.questionId);
      this.loadQuestionData(val.questionId);
    });
  }

  private loadQuestionData(questionId: string) {
    this.question = this.apollo.watchQuery<getQuestionView, getQuestionViewVariables>({
      query: QUESTION_VIEW_QUERY,
      variables: {
        id: questionId,
      }
    }).valueChanges.pipe(map(val => val.data.question));
  }

  sortDesc(arr: getQuestionView_question_answers[]) {
    const sorted = [...arr].sort((a, b) => new Date(a.createdOn).valueOf() - new Date(b.createdOn).valueOf());
    return sorted;
  }

}
