"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const typeorm_1 = require("typeorm");
const answer_model_1 = require("../models/answer.model");
const answer_repository_1 = require("../repositories/answer.repository");
const question_service_1 = require("./question.service");
let AnswerService = class AnswerService {
    constructor() {
        this.repository = typeorm_1.getCustomRepository(answer_repository_1.AnswerRepository);
    }
    findById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.repository.findOne(id);
        });
    }
    addAnswer(questionId, answerText) {
        return __awaiter(this, void 0, void 0, function* () {
            const question = yield this.questionService.findById(questionId);
            if (!question) {
                return undefined;
            }
            const answer = new answer_model_1.Answer();
            answer.response = answerText;
            yield (answer.question = Promise.resolve(question));
            this.repository.save(answer);
            return this.questionService.findById(questionId);
        });
    }
    vote(id, upvote) {
        return __awaiter(this, void 0, void 0, function* () {
            const answer = yield this.findById(id);
            upvote ? answer.votes++ : answer.votes--;
            return this.repository.save(answer);
        });
    }
};
__decorate([
    typedi_1.Inject(() => question_service_1.QuestionService),
    __metadata("design:type", question_service_1.QuestionService)
], AnswerService.prototype, "questionService", void 0);
AnswerService = __decorate([
    typedi_1.Service()
], AnswerService);
exports.AnswerService = AnswerService;
//# sourceMappingURL=answer.service.js.map