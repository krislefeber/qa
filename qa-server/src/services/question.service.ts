import { Inject, Service } from 'typedi';
import { getCustomRepository } from 'typeorm';
import { Account } from '../models/account.model';
import { Question } from '../models/question.model';
import { Tag } from '../models/tag.model';
import { QuestionRepository } from '../repositories/question.repository';
import { QuestionSearchInput } from '../resolvers/question/question-search.input';
import AsyncArray, { isUUID } from '../util/util';
import { ElasticService } from './elastic.service';
import { TagService } from './tag.service';

@Service()
export class QuestionService {

  @Inject(() => TagService)
  private tagService: TagService;
  @Inject(() => ElasticService)
  private elasticSearch: ElasticService;

  private repository = getCustomRepository(QuestionRepository);

  public async createQuestion(createdBy: Account, title: string, description: string) {
    const question = new Question();
    await (question.createdBy = Promise.resolve(createdBy));
    question.title = title;
    question.description = description;

    const dbQuestion = await this.repository.save(question);
    this.elasticSearch.indexQuestion(dbQuestion);
    return dbQuestion;

  }

  public async findById(id: string) {
    return this.repository.findOne(id);
  }

  public async addTag(tagSearch: string, questionId: string) {
      const question = await this.findById(questionId);

      if (!question) {
        throw new  Error('Question not found.');
      }

      let tag = isUUID(tagSearch) ? await this.tagService.findById(tagSearch) : undefined;

      if (!tag) {
        tag = await this.tagService.create(tagSearch);
      }

      await this.repository.createQueryBuilder('question')
      .relation(Question, 'tags')
      .of(question)
      .add(tag);
      return this.findById(questionId);

  }

  public async removeTag(tagId: string, questionId: string) {
    await this.repository.createQueryBuilder('question')
    .relation(Question, 'tags')
    .of(await this.findById(questionId))
    .remove(tagId);
  }

  public async search(input: QuestionSearchInput): Promise<Question[]> {
    return AsyncArray.from(await this.elasticSearch.searchQuestions(input.search))
                     .asyncMap((id) => this.findById(id));
  }
}
