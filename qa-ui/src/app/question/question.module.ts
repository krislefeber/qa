import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QuestionRoutingModule } from './question-routing.module';
import { QuestionViewComponent } from './question-view/question-view.component';
import { QuestionComponent } from './question-view/question/question.component';
import { ResponseComponent } from './question-view/response/response.component';
import { CommentComponent } from './question-view/comment/comment.component';
import { MatCardModule, MatInputModule, MatButtonModule, MatIconModule, MatTooltipModule, MatMenuModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { TagsModule } from '../tags/tags.module';
@NgModule({
  declarations: [QuestionViewComponent, QuestionComponent, ResponseComponent, CommentComponent],
  imports: [
    CommonModule,
    QuestionRoutingModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    TagsModule,
    // material
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatTooltipModule
  ]
})
export class QuestionModule { }
