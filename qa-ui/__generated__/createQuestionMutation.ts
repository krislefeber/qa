

/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: createQuestionMutation
// ====================================================

export interface createQuestionMutation_createQuestion_tags {
  __typename: "Tag";
  id: string;
  name: string;
}

export interface createQuestionMutation_createQuestion_answers {
  __typename: "Answer";
  id: string;
  votes: number;
  response: string;
}

export interface createQuestionMutation_createQuestion {
  __typename: "Question";
  id: string;
  title: string;
  description: string;
  votes: number;
  tags: createQuestionMutation_createQuestion_tags[] | null;
  answers: createQuestionMutation_createQuestion_answers[] | null;
}

export interface createQuestionMutation {
  createQuestion: createQuestionMutation_createQuestion;
}

export interface createQuestionMutationVariables {
  title: string;
  description: string;
}

/* tslint:disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

/**
 * 
 */
export interface QuestionSearchInput {
  search?: string | null;
}

//==============================================================
// END Enums and Input Objects
//==============================================================