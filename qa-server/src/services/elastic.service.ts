import { Client as ElasticClient} from 'elasticsearch';
import { Service } from 'typedi';
import { Question } from '../models/question.model';

@Service()
export class ElasticService {

  private client: ElasticClient;
  constructor() {
    const BONSAI_URL = process.env.BONSAI_URL;

    this.client = new ElasticClient({
      host: BONSAI_URL,
      log: 'error',

    });
    this.client.indices.exists({
      index: 'questions',
    }, (err, result) => {
      if (!result) {
        this.client.indices.create({
          index: 'questions',
        });
      }
    });

  }

  public async indexQuestion(question: Question) {

    const tags = (await question.tags).map((tag) => tag.name);

    this.client.create({
      id: question.id,

      body: {
        title: question.title,
        description: question.description,
        tags,
      },
      index: 'questions',
      type: 'question',
    });
  }

  public async searchQuestions(query: string): Promise<string[]> {
    const results = await this.client.search({
      q: query,
      index: 'questions',
      type: 'question',
    });
    return results.hits.hits.map((hit) => hit._id);
  }
}
