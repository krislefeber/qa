

/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: searchQuestions
// ====================================================

export interface searchQuestions_search {
  __typename: "Question";
  id: string;
  title: string;
  description: string;
  numberOfAnswers: number;
  votes: number;
}

export interface searchQuestions {
  search: searchQuestions_search[];
}

export interface searchQuestionsVariables {
  searchInput: QuestionSearchInput;
}

/* tslint:disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

/**
 * 
 */
export interface QuestionSearchInput {
  search?: string | null;
}

//==============================================================
// END Enums and Input Objects
//==============================================================