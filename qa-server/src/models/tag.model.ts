import { Field, ID, ObjectType } from 'type-graphql';
import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Question } from './question.model';
import { SoftDeletable } from './softdeletable';

@ObjectType({ implements: SoftDeletable})
@Entity()
export class Tag extends SoftDeletable {

  @Field(() => ID)
  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @Field()
  @Column({
    unique: true,
  })
  public name: string;

  constructor(name: string) {
    super();
    this.name = name;
  }
}
