import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-add-tag',
  templateUrl: './add-tag.component.html',
  styleUrls: ['./add-tag.component.scss']
})
export class AddTagComponent implements OnInit {

  @Output() tagAdded: EventEmitter<string> = new EventEmitter();
  isEditable: boolean;
  constructor() { }

  ngOnInit() {
  }

  toggleEditable() {
    this.isEditable = !this.isEditable;
  }
}
