export class NoEntityFoundException extends Error {

    constructor(object: new() => object, id: string) {
        super(`Unable to find entity in database for ${new object().constructor.name} with id of ${id}`);
    }
}
