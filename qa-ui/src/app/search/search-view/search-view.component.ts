import { Component, OnInit } from '@angular/core';
import { Search } from './search-form/search-form.component';
import { Apollo } from 'apollo-angular';
import { BehaviorSubject } from 'rxjs';
import { SEARCH_QUERY } from './search-query.graphql';
import { searchQuestions, searchQuestionsVariables, searchQuestions_search } from '__generated__/searchQuestions';
import { map, tap, first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { QuestionService } from 'src/app/services/question.service';

@Component({
  selector: 'app-search-view',
  templateUrl: './search-view.component.html',
  styleUrls: ['./search-view.component.scss']
})
export class SearchViewComponent implements OnInit {

  searchTriggered = false;
  searchResults: BehaviorSubject<searchQuestions_search[]> = new BehaviorSubject([]);
  showQuestion = false;

  constructor(private apollo: Apollo,
              private router: Router,
              private route: ActivatedRoute,
              private questionService: QuestionService) { }

  ngOnInit() {
    this.route.queryParams.subscribe((val: Search) => {
      if (val) {
        this.triggerSearch(val);
      }
    });

    this.questionService.currentSearchObservable().subscribe(currentSearch => {

      console.log('currentSearch', currentSearch);
      this.triggerSearch(currentSearch);
    });
  }

  triggerSearch(search: Search) {
    console.log(search);
    this.apollo.query<searchQuestions, searchQuestionsVariables>({
      query: SEARCH_QUERY,
      variables: {
        searchInput: {
          search: !search.search || search.search.length === 0 ? '*' : search.search
        }
      }
    }).pipe(map(response => response.data.search),
    tap(vals => {
      this.searchTriggered = true;
      this.searchResults.next(vals);
      this.updateRoute(search);
    })).subscribe();
  }

  private updateRoute(search: Search) {
    this.router.navigate(['./'], {
      relativeTo: this.route,
      queryParams: search,
      skipLocationChange: false
    });
  }
}
