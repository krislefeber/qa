"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const elastic = require("elasticsearch");
class ElasticService {
    constructor() {
        let bonsai_url = process.env.BONSAI_URL;
        let client = new elastic.Client({
            host: bonsai_url,
            log: 'trace',
        });
        client.ping({
            requestTimeout: 30000
        }, (error) => {
            if (error) {
                console.error("elasticsearch cluster is down!");
            }
            else {
                console.log("All is well");
            }
        });
    }
}
exports.ElasticService = ElasticService;
//# sourceMappingURL=elastic.service.js.map