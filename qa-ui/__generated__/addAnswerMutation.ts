

/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: addAnswerMutation
// ====================================================

export interface addAnswerMutation_answerQuestion_tags {
  __typename: "Tag";
  id: string;
  name: string;
}

export interface addAnswerMutation_answerQuestion_createdBy {
  __typename: "Account";
  id: string;
}

export interface addAnswerMutation_answerQuestion_answers_createdBy {
  __typename: "Account";
  id: string;
}

export interface addAnswerMutation_answerQuestion_answers {
  __typename: "Answer";
  id: string;
  createdOn: any;
  votes: number;
  response: string;
  createdBy: addAnswerMutation_answerQuestion_answers_createdBy | null;
}

export interface addAnswerMutation_answerQuestion {
  __typename: "Question";
  id: string;
  createdOn: any;
  title: string;
  description: string;
  votes: number;
  tags: addAnswerMutation_answerQuestion_tags[] | null;
  createdBy: addAnswerMutation_answerQuestion_createdBy | null;
  answers: addAnswerMutation_answerQuestion_answers[] | null;
}

export interface addAnswerMutation {
  answerQuestion: addAnswerMutation_answerQuestion;
}

export interface addAnswerMutationVariables {
  questionId: string;
  answer: string;
}

/* tslint:disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

/**
 * 
 */
export interface QuestionSearchInput {
  search?: string | null;
}

//==============================================================
// END Enums and Input Objects
//==============================================================