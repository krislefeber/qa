export const environment = {
  production: true,
  graphqlEndpoint: 'http://localhost:4200/graphql'
};
