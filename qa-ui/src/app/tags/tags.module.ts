import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TagViewComponent } from './tag-view/tag-view.component';
import { AddTagComponent } from './add-tag/add-tag.component';
import { MatChipsModule, MatIconModule, MatButtonModule, MatInputModule, MatTooltipModule } from '@angular/material';
import { TagListComponent } from './tag-list/tag-list.component';

@NgModule({
  declarations: [TagViewComponent, AddTagComponent, TagListComponent],
  imports: [
    CommonModule,

    // Material
    MatButtonModule,
    MatChipsModule,
    MatIconModule,
    MatInputModule,
    MatTooltipModule
  ],
  exports: [
    TagListComponent,
  ]
})
export class TagsModule { }
