import { Field, ID, ObjectType } from 'type-graphql';
import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Question } from './question.model';
@ObjectType()
@Entity()
export class Account {

  @Field(() => ID)
  @PrimaryGeneratedColumn('uuid')
  public id: string;
}
