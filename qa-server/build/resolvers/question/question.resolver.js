"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const type_graphql_1 = require("type-graphql");
const typedi_1 = require("typedi");
const NoEntityFoundException_1 = require("../../exceptions/NoEntityFoundException");
const question_model_1 = require("../../models/question.model");
const question_service_1 = require("../../services/question.service");
const question_search_input_1 = require("./question-search.input");
let QuestionResolver = class QuestionResolver {
    question(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const account = yield this.questionService.findById(id);
            if (account === undefined) {
                throw new NoEntityFoundException_1.NoEntityFoundException(question_model_1.Question, id);
            }
            return account;
        });
    }
    createQuestion(title, description) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.questionService.createQuestion(undefined, title, description);
        });
    }
    addTag(tag, questionId) {
        return __awaiter(this, void 0, void 0, function* () {
            this.questionService.addTag(tag, questionId);
        });
    }
    search(searchInput) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.questionService.search(searchInput);
        });
    }
};
__decorate([
    typedi_1.Inject(() => question_service_1.QuestionService),
    __metadata("design:type", question_service_1.QuestionService)
], QuestionResolver.prototype, "questionService", void 0);
__decorate([
    type_graphql_1.Query(() => question_model_1.Question),
    __param(0, type_graphql_1.Arg('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], QuestionResolver.prototype, "question", null);
__decorate([
    type_graphql_1.Mutation(() => question_model_1.Question),
    __param(0, type_graphql_1.Arg('title')), __param(1, type_graphql_1.Arg('description')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", Promise)
], QuestionResolver.prototype, "createQuestion", null);
__decorate([
    type_graphql_1.Mutation(() => question_model_1.Question),
    __param(0, type_graphql_1.Arg('tagId', {
        description: 'The ID of an existing tag or the name of a new tag',
    })), __param(1, type_graphql_1.Arg('questionId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", Promise)
], QuestionResolver.prototype, "addTag", null);
__decorate([
    type_graphql_1.Query(() => [question_model_1.Question]),
    __param(0, type_graphql_1.Arg('searchInpt')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [question_search_input_1.QuestionSearchInput]),
    __metadata("design:returntype", Promise)
], QuestionResolver.prototype, "search", null);
QuestionResolver = __decorate([
    type_graphql_1.Resolver(() => question_model_1.Question)
], QuestionResolver);
exports.QuestionResolver = QuestionResolver;
//# sourceMappingURL=question.resolver.js.map