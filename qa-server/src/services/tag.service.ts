import { Service } from 'typedi';
import { getCustomRepository } from 'typeorm';
import { Tag } from '../models/tag.model';
import { TagRepository } from '../repositories/tag.repository';
import { isUUID } from '../util/util';

@Service()
export class TagService {
  private repository = getCustomRepository(TagRepository);

  public async findById(id: string) {
    return this.repository.findOne(id);
  }

  public async find(tag: string): Promise<Tag> {
    if (isUUID(tag))  {
      return this.findById(tag);
    }
    return await this.repository.findByName(tag);
  }

  public async create(name: string) {
    return this.repository.save(new Tag(name));
  }

}
