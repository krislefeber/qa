import { Inject, Service } from 'typedi';
import { getCustomRepository } from 'typeorm';
import { Answer } from '../models/answer.model';
import { AnswerRepository } from '../repositories/answer.repository';
import { QuestionService } from './question.service';

@Service()
export class AnswerService {

  @Inject(() => QuestionService)
  private questionService: QuestionService;

  private repository = getCustomRepository(AnswerRepository);

  public async findById(id: string) {
    return this.repository.findOne(id);
  }

  public async addAnswer(questionId: string, answerText: string) {
    const question = await this.questionService.findById(questionId);

    if (!question) {
      return undefined;
    }

    const answer = new Answer();
    answer.response = answerText;
    await (answer.question = Promise.resolve(question));

    this.repository.save(answer);
    return this.questionService.findById(questionId);
  }

  public async vote(id: string, upvote: boolean): Promise<Answer> {
    const answer = await this.findById(id);

    upvote ? answer.votes++ : answer.votes--;
    return this.repository.save(answer);
  }
}
