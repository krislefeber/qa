import { Component } from '@angular/core';
import { Search } from './search/search-view/search-form/search-form.component';
import { Router } from '@angular/router';
import { QuestionService } from './services/question.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private router: Router,
              private questionService: QuestionService) {

  }


  triggerSearch(search: Search) {

    this.questionService.updateSearch(search);
  }

  showQuestion() {
    this.router.navigate(['/search/ask']);
  }
}
