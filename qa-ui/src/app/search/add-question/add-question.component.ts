import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Apollo } from 'apollo-angular';
import { ADD_QUESTION_MUTATION } from './add-question.graphql';
import { createQuestionMutation, createQuestionMutationVariables } from '__generated__/createQuestionMutation';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-question',
  templateUrl: './add-question.component.html',
  styleUrls: ['./add-question.component.scss']
})
export class AddQuestionComponent implements OnInit {

  questionForm: FormGroup;

  @Output() hideQuestion = new EventEmitter();

  constructor(private formBuilder: FormBuilder,
              private apollo: Apollo,
              private router: Router) { }

  ngOnInit() {
    this.questionForm = this.formBuilder.group({
      subject: ['', [Validators.required, Validators.minLength(15), Validators.maxLength(200)]],
      description: ['', [Validators.required, Validators.minLength(20)]]
    });
  }

  cancelQuestion() {
    this.hideQuestion.emit();
  }

  submitQuestion() {

    this.apollo.mutate<createQuestionMutation, createQuestionMutationVariables>({
      mutation: ADD_QUESTION_MUTATION,
      variables: {
        title: this.questionForm.get('subject').value,
        description: this.questionForm.get('description').value
      }
    }).subscribe(result => {
      this.router.navigate(['/questions/' + result.data.createQuestion.id]);
    });
  }
}
