"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_yoga_1 = require("graphql-yoga");
require("reflect-metadata");
const type_graphql_1 = require("type-graphql");
const typedi_1 = require("typedi");
const typeorm_1 = require("typeorm");
const winston = require("winston");
const question_resolver_1 = require("./resolvers/question/question.resolver");
const answer_resolver_1 = require("./resolvers/answer/answer.resolver");
function bootstrap() {
    return __awaiter(this, void 0, void 0, function* () {
        winston.info('building schema...');
        const schema = yield type_graphql_1.buildSchema({
            resolvers: [
                answer_resolver_1.AnswerResolver,
                question_resolver_1.QuestionResolver,
            ],
        });
        winston.info('Schema successfully built.');
        winston.info('Creating Server...');
        const server = new graphql_yoga_1.GraphQLServer({
            schema,
        });
        const serverOptions = {
            cors: {
                credentials: true,
                origin: ['https://lefesoft.com', 'https://staging.lefesoft.com', 'http://localhost:4200'],
            },
            debug: !process.env.PROD,
            endpoint: '/graphql',
            playground: '/playground',
            port: Number.parseInt(process.env.PORT) || 3900,
        };
        winston.info('Server successfully built.');
        winston.info('Starting server...');
        return server.start(serverOptions, (s) => {
            winston.info(`Server is running, GraphQL Playground available at http://localhost:${s.port}${s.playground}`);
            return s;
        });
    });
}
function setupLogger() {
    return __awaiter(this, void 0, void 0, function* () {
        winston.info('Winston Successfully configured');
    });
}
const start = () => __awaiter(this, void 0, void 0, function* () {
    yield typeorm_1.createConnection();
    type_graphql_1.useContainer(typedi_1.Container);
    yield setupLogger();
    winston.info('Connection created at ' + new Date());
    const server = yield bootstrap();
    server.on('error', (args) => winston.error(args.message));
});
start();
//# sourceMappingURL=index.js.map