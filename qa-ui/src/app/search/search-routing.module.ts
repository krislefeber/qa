import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchViewComponent } from './search-view/search-view.component';
import { AddQuestionComponent } from './add-question/add-question.component';

const routes: Routes = [
  {
    component: SearchViewComponent,
    path: '',
    pathMatch: 'full'
  },
  {
    component: AddQuestionComponent,
    path: 'ask'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchRoutingModule { }
