import gql from 'graphql-tag';


export const SEARCH_QUERY = gql`
query searchQuestions($searchInput: QuestionSearchInput!){
  search(searchInpt: $searchInput) {
    id
    title
    description
    numberOfAnswers
    votes
  }
}
`;
