"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var Answer_1;
"use strict";
const type_graphql_1 = require("type-graphql");
const typeorm_1 = require("typeorm");
const account_model_1 = require("./account.model");
const question_model_1 = require("./question.model");
const softdeletable_1 = require("./softdeletable");
let Answer = Answer_1 = class Answer extends softdeletable_1.SoftDeletable {
};
__decorate([
    type_graphql_1.Field(() => type_graphql_1.ID),
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Answer.prototype, "id", void 0);
__decorate([
    type_graphql_1.Field(() => account_model_1.Account, { nullable: true }),
    typeorm_1.ManyToOne((type) => account_model_1.Account, { lazy: true, nullable: true }),
    __metadata("design:type", Promise)
], Answer.prototype, "createdBy", void 0);
__decorate([
    type_graphql_1.Field(() => [Answer_1]),
    typeorm_1.ManyToOne((type) => question_model_1.Question, (question) => question.answers, { lazy: true }),
    __metadata("design:type", Promise)
], Answer.prototype, "question", void 0);
__decorate([
    type_graphql_1.Field(),
    typeorm_1.Column({ default: 0 }),
    __metadata("design:type", Number)
], Answer.prototype, "votes", void 0);
__decorate([
    type_graphql_1.Field(),
    typeorm_1.Column(),
    __metadata("design:type", String)
], Answer.prototype, "response", void 0);
Answer = Answer_1 = __decorate([
    type_graphql_1.ObjectType({ implements: softdeletable_1.SoftDeletable }),
    typeorm_1.Entity()
], Answer);
exports.Answer = Answer;
//# sourceMappingURL=answer.model.js.map