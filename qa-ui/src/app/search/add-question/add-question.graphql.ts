import gql from 'graphql-tag';


export const ADD_QUESTION_MUTATION = gql`
mutation createQuestionMutation($title:String!, $description:String!) {
  createQuestion(title: $title,
  							description: $description) {
    id
    title
    description
    votes
    tags {
      id
      name
    }
    answers {
      id
      votes
      response
    }
  }
}
`;
