import { Field, InterfaceType } from 'type-graphql';
import { Column, CreateDateColumn } from 'typeorm';
@InterfaceType()
export abstract class SoftDeletable {

  @Column({default: false})
  public softDeleted?: boolean;

  @Field()
  @CreateDateColumn({type: 'timestamp with time zone', default: new Date()})
  public createdOn: Date;
}
