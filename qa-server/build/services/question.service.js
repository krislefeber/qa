"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const typeorm_1 = require("typeorm");
const question_model_1 = require("../models/question.model");
const question_repository_1 = require("../repositories/question.repository");
const tag_service_1 = require("./tag.service");
const tag_model_1 = require("../models/tag.model");
let QuestionService = class QuestionService {
    constructor() {
        this.repository = typeorm_1.getCustomRepository(question_repository_1.QuestionRepository);
    }
    createQuestion(createdBy, title, description) {
        return __awaiter(this, void 0, void 0, function* () {
            const question = new question_model_1.Question();
            yield (question.createdBy = Promise.resolve(createdBy));
            question.title = title;
            question.description = description;
            return this.repository.save(question);
        });
    }
    findById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.repository.findOne(id);
        });
    }
    addTag(tagSearch, questionId) {
        return __awaiter(this, void 0, void 0, function* () {
            const question = yield this.findById(questionId);
            if (!question) {
                throw new Error('Question not found.');
            }
            let tag = yield this.tagService.findById(tagSearch);
            if (!tag) {
                tag = new tag_model_1.Tag(tagSearch);
            }
            let questionTags = yield question.tags;
            questionTags = [...questionTags, tag];
            yield (question.tags = Promise.resolve(questionTags));
            return this.repository.save(question);
        });
    }
    search(input) {
        return __awaiter(this, void 0, void 0, function* () {
            return;
        });
    }
};
__decorate([
    typedi_1.Inject(() => tag_service_1.TagService),
    __metadata("design:type", tag_service_1.TagService)
], QuestionService.prototype, "tagService", void 0);
QuestionService = __decorate([
    typedi_1.Service()
], QuestionService);
exports.QuestionService = QuestionService;
//# sourceMappingURL=question.service.js.map