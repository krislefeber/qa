

/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: QuestionView
// ====================================================

export interface QuestionView_tags {
  __typename: "Tag";
  id: string;
  name: string;
}

export interface QuestionView_createdBy {
  __typename: "Account";
  id: string;
}

export interface QuestionView_answers_createdBy {
  __typename: "Account";
  id: string;
}

export interface QuestionView_answers {
  __typename: "Answer";
  id: string;
  createdOn: any;
  votes: number;
  response: string;
  createdBy: QuestionView_answers_createdBy | null;
}

export interface QuestionView {
  __typename: "Question";
  id: string;
  createdOn: any;
  title: string;
  description: string;
  votes: number;
  tags: QuestionView_tags[] | null;
  createdBy: QuestionView_createdBy | null;
  answers: QuestionView_answers[] | null;
}

/* tslint:disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

/**
 * 
 */
export interface QuestionSearchInput {
  search?: string | null;
}

//==============================================================
// END Enums and Input Objects
//==============================================================