"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const type_graphql_1 = require("type-graphql");
const typedi_1 = require("typedi");
const answer_model_1 = require("../../models/answer.model");
const question_model_1 = require("../../models/question.model");
const answer_service_1 = require("../../services/answer.service");
class AnswerResolver {
    answerQuestion(questionId, answer) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.answerService.addAnswer(questionId, answer);
        });
    }
    upVoteAnswer(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.answerService.vote(id, true);
        });
    }
    downVoteAnswer(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.answerService.vote(id, false);
        });
    }
}
__decorate([
    typedi_1.Inject(() => answer_service_1.AnswerService),
    __metadata("design:type", answer_service_1.AnswerService)
], AnswerResolver.prototype, "answerService", void 0);
__decorate([
    type_graphql_1.Mutation(() => question_model_1.Question),
    __param(0, type_graphql_1.Arg('questionId')), __param(1, type_graphql_1.Arg('answer')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", Promise)
], AnswerResolver.prototype, "answerQuestion", null);
__decorate([
    type_graphql_1.Mutation(() => answer_model_1.Answer),
    __param(0, type_graphql_1.Arg('answerId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AnswerResolver.prototype, "upVoteAnswer", null);
__decorate([
    type_graphql_1.Mutation(() => answer_model_1.Answer),
    __param(0, type_graphql_1.Arg('answerId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AnswerResolver.prototype, "downVoteAnswer", null);
exports.AnswerResolver = AnswerResolver;
//# sourceMappingURL=answer.resolver.js.map