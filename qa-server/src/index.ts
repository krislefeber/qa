import { GraphQLServer, Options } from 'graphql-yoga';
import 'reflect-metadata';
import { buildSchema, useContainer } from 'type-graphql';
import { Container } from 'typedi';
import { createConnection } from 'typeorm';
import * as winston from 'winston';
import { AnswerResolver } from './resolvers/answer/answer.resolver';
import { QuestionResolver } from './resolvers/question/question.resolver';

async function bootstrap() {
    // Building schema here...
    winston.info('building schema...');
    const schema = await buildSchema({
        resolvers: [
          AnswerResolver,
          QuestionResolver,
        ],
    });

    winston.info('Schema successfully built.');
    winston.info('Creating Server...');

    // Create GraphQL server
    const server = new GraphQLServer({
        // context: async ({ request }) => {
        //     if (!request.headers.authorization) {
        //         return false;
        //     }

        //     const usernameAndPassword = Buffer.from(request.headers.authorization.split(' ')[1], 'base64')
        //                                             .toString().split(':');
        //     const account = await new AccountService().findAccount(usernameAndPassword[0],
        //                                                                    usernameAndPassword[1]);

        //     const ctx: Context = {
        //         user: account,
        //     };
        //     return ctx;
        // },
        schema,
    });

    // Configure server options
    const serverOptions: Options = {
        cors: {
          credentials: true,
          origin: ['https://lefesoft.com', 'https://staging.lefesoft.com', 'http://localhost:4200'],
        } ,
        debug: !process.env.PROD,
        endpoint: '/graphql',
        playground: '/playground',
        port: Number.parseInt(process.env.PORT) || 3900,
    };

    winston.info('Server successfully built.');
    winston.info('Starting server...');
    // Start the server
    return server.start(serverOptions, (s) => {
        winston.info(
            `Server is running, GraphQL Playground available at http://localhost:${s.port}${s.playground}`,
        );
        return s;
    });
}

async function setupLogger() {
   // winston.configure( {level: 'debug'});
    winston.info('Winston Successfully configured');
}

const start = async () => {
  await createConnection();
  useContainer(Container);
  await setupLogger();
  winston.info('Connection created at ' + new Date());
  const server = await bootstrap();
  server.on('error', (args) => winston.error(args.message));

};
start();
