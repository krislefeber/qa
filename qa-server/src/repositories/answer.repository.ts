import { EntityRepository } from 'typeorm';
import { Answer } from '../models/answer.model';
import { BaseRepository } from './BaseRepository';

@EntityRepository(Answer)
export class AnswerRepository extends BaseRepository<Answer> {

}
