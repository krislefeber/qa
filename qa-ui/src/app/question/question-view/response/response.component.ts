import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { getQuestionView_question_answers } from '__generated__/getQuestionView';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Apollo } from 'apollo-angular';
import { addAnswerMutation, addAnswerMutationVariables } from '__generated__/addAnswerMutation';
import { ANSWER_QUESTION_MUTATION, UPVOTE_ANSWER_MUTATION, DOWNVOTE_ANSWER_MUTATION, QUESTION_VIEW_QUERY } from '../question-view.graphql';
import { upVoteAnswer, upVoteAnswerVariables } from '__generated__/upVoteAnswer';

@Component({
  selector: 'app-response',
  templateUrl: './response.component.html',
  styleUrls: ['./response.component.scss']
})
export class ResponseComponent implements OnInit {

  @Input() response: getQuestionView_question_answers;

  @Input() questionId: string;

  responseForm: FormGroup;
  answerSubmitted = false;

  @Input() editable: boolean;
  constructor(private formBuilder: FormBuilder,
              private apollo: Apollo) { }

  ngOnInit() {
    this.responseForm = this.formBuilder.group({
      response: ['', [Validators.required]]
    });
  }

  get hasAResponse(): boolean {
    const val = this.responseForm.get('reponse').value;
    return !!val && val.length > 0;
  }

  submitAnswer() {
    this.apollo.mutate<addAnswerMutation, addAnswerMutationVariables>({
      mutation: ANSWER_QUESTION_MUTATION,
      variables: {
        answer: this.responseForm.get('response').value,
        questionId: this.questionId,
      }
    }).subscribe(() => { this.answerSubmitted = true; this.responseForm.reset();
                         this.responseForm.markAsPristine(); this.responseForm.markAsUntouched(); });
  }

  get isPositive(): boolean {
    return this.response.votes >= 0;
  }

  upvote() {
    this.apollo.mutate<upVoteAnswer, upVoteAnswerVariables>({
      mutation: UPVOTE_ANSWER_MUTATION,
      variables: {
        answerId: this.response.id
      }
    }).subscribe();
  }

  downvote() {
    this.apollo.mutate<upVoteAnswer, upVoteAnswerVariables>({
      mutation: DOWNVOTE_ANSWER_MUTATION,
      variables: {
        answerId: this.response.id
      }
    }).subscribe();
  }

}
