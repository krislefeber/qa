import { Component, OnInit, Input } from '@angular/core';
import { Tag } from 'src/app/tags/tag-view/tag-view.component';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {

  @Input() title: string;
  @Input() description: string;
  @Input() tags: Tag[];
  constructor() { }

  ngOnInit() {
  }

  deleteTag(tag: Tag) {
    console.log('mock deleting tag:', tag.id);
  }

}
