

/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: getQuestionView
// ====================================================

export interface getQuestionView_question_tags {
  __typename: "Tag";
  id: string;
  name: string;
}

export interface getQuestionView_question_createdBy {
  __typename: "Account";
  id: string;
}

export interface getQuestionView_question_answers_createdBy {
  __typename: "Account";
  id: string;
}

export interface getQuestionView_question_answers {
  __typename: "Answer";
  id: string;
  createdOn: any;
  votes: number;
  response: string;
  createdBy: getQuestionView_question_answers_createdBy | null;
}

export interface getQuestionView_question {
  __typename: "Question";
  id: string;
  createdOn: any;
  title: string;
  description: string;
  votes: number;
  tags: getQuestionView_question_tags[] | null;
  createdBy: getQuestionView_question_createdBy | null;
  answers: getQuestionView_question_answers[] | null;
}

export interface getQuestionView {
  question: getQuestionView_question;
}

export interface getQuestionViewVariables {
  id: string;
}

/* tslint:disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

/**
 * 
 */
export interface QuestionSearchInput {
  search?: string | null;
}

//==============================================================
// END Enums and Input Objects
//==============================================================