

/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: upVoteAnswer
// ====================================================

export interface upVoteAnswer_upVoteAnswer_createdBy {
  __typename: "Account";
  id: string;
}

export interface upVoteAnswer_upVoteAnswer {
  __typename: "Answer";
  id: string;
  createdOn: any;
  votes: number;
  response: string;
  createdBy: upVoteAnswer_upVoteAnswer_createdBy | null;
}

export interface upVoteAnswer {
  upVoteAnswer: upVoteAnswer_upVoteAnswer;
}

export interface upVoteAnswerVariables {
  answerId: string;
}

/* tslint:disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

/**
 * 
 */
export interface QuestionSearchInput {
  search?: string | null;
}

//==============================================================
// END Enums and Input Objects
//==============================================================