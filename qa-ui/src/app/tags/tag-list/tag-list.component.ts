import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Tag } from '../tag-view/tag-view.component';
import { Observable } from 'apollo-link';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-tag-list',
  templateUrl: './tag-list.component.html',
  styleUrls: ['./tag-list.component.scss']
})
export class TagListComponent implements OnInit {

  @Input() tags: Tag[];
  @Output() tagRemoved: EventEmitter<Tag> = new EventEmitter();
  isClicked: BehaviorSubject<Tag> = new BehaviorSubject({id: '', name: ''});
  constructor() { }

  ngOnInit() {
  }

  handleTagRemoved(tag: Tag) {
    this.tagRemoved.emit(tag);
  }

  handleClick(tag: Tag) {
    this.isClicked.next(tag);
  }
}
