"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class NoEntityFoundException extends Error {
    constructor(object, id) {
        super(`Unable to find entity in database for ${new object().constructor.name} with id of ${id}`);
    }
}
exports.NoEntityFoundException = NoEntityFoundException;
//# sourceMappingURL=NoEntityFoundException.js.map