import { Field, ID, ObjectType } from 'type-graphql';
import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Account } from './account.model';
import { Question } from './question.model';
import { SoftDeletable } from './softdeletable';

@ObjectType({ implements: SoftDeletable})
@Entity()
export class Answer extends SoftDeletable {

  @Field(() => ID)
  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @Field(() => Account, { nullable: true })
  @ManyToOne((type) => Account, {lazy: true, nullable: true})
  public createdBy: Promise<Account>;

  @Field(() => [Answer])
  @ManyToOne((type) => Question, (question) => question.answers, {lazy: true})
  public question: Promise<Question>;

  @Field()
  @Column({ default: 0 })
  public votes: number;

  @Field()
  @Column()
  public response: string;
}
